
//# sourceURL=projectReports/projectReportsSiteConfig.js

if (typeof CCF === 'undefined') {
	CCF = {};
}

if (typeof CCF.prsiteconfig === 'undefined') {
	CCF.prsiteconfig = { };
}

CCF.prsiteconfig.populateScriptsSiteConfigTable = function(tag) {

	CCF.prsiteconfig.currentTag = tag;
	CCF.prsiteconfig.projectReportsNS = [];
	CCF.prsiteconfig.configuredNS = [];
	CCF.prsiteconfig.projectList = [];
	CCF.prsiteconfig.initProjectList();

	XNAT.xhr.getJSON({
    		url: XNAT.url.rootUrl('/xapi/spawner/namespaces'),
    		success: function(nsJson){
			nsJson.sort();
       			$.each(nsJson, function(){
				var NS = this.toString();
				if (NS.includes(":projectReport")) {
					CCF.prsiteconfig.projectReportsNS.push(NS);
				}
			});
			XNAT.xhr.getJSON({
    				url: XNAT.url.rootUrl('/data/config/projectReportsSiteConfig/configuration?contents=true'),
    				success: function(configJson){
					var removed = true;
					outerLoop:
					while (removed) {
						for (var i=0; i<configJson.length; i++) {
							var rptConfig = configJson[i];
							if (typeof rptConfig.namespace !== 'undefined') {
								if (!CCF.prsiteconfig.projectReportsNS.includes(rptConfig.namespace)) {
									configJson.splice(i,1);
									break outerLoop;
								}
							}
						} 
						removed = false;
					}
					var added = false;
					for (var i=0; i<CCF.prsiteconfig.projectReportsNS.length; i++) {
						var prNS = CCF.prsiteconfig.projectReportsNS[i];
						var hasMatch = false;
						for (var j=0; j<configJson.length; j++) {
							var rptConfig = configJson[j];
							if (typeof rptConfig.namespace !== 'undefined' && rptConfig.namespace == prNS) {
								hasMatch = true;
								break;
							}
						} 
						if (!hasMatch) {
							var newConfig = { enabled: false, namespace: prNS, projectList: [], description: "" };
							added = true;
							configJson.push(newConfig);
						}
					}
					if (added) {
						CCF.prsiteconfig.updateSiteConfig(configJson);
					} else {
						CCF.prsiteconfig.configuration = configJson;
						CCF.prsiteconfig.populateTable();
					}
				},
    				failure: function(failData){
					if (failData.status==404) {
						CCF.prsiteconfig.initSiteConfig();
						CCF.prsiteconfig.populateTable();
					}
				}
			});
		}
	});

}

CCF.prsiteconfig.initProjectList = function() {

	XNAT.xhr.getJSON({
    		url: XNAT.url.rootUrl('/data/projects?format=json'),
    		success: function(projList){
			if (typeof projList.ResultSet !== 'undefined' && typeof projList.ResultSet.Result !== 'undefined') {
       				$.each(projList.ResultSet.Result, function(){
					var projInfo = this;
					if (typeof projInfo.ID !== 'undefined') {
						CCF.prsiteconfig.projectList.push(projInfo.ID.toString());
					}
				})
				CCF.prsiteconfig.projectList.sort();
			}
		}
	});

}

CCF.prsiteconfig.initSiteConfig = function() {

	CCF.prsiteconfig.configuration = [];
       	$.each(CCF.prsiteconfig.projectReportsNS, function(){
		var NS = this;
		var config = { enabled: false, namespace: NS, projectList: [], description: "" };
		CCF.prsiteconfig.configuration.push(config);
	});
	CCF.prsiteconfig.postSiteConfig();

}

CCF.prsiteconfig.updateSiteConfig = function(configJson) {

	CCF.prsiteconfig.configuration = configJson;
	CCF.prsiteconfig.postSiteConfig();

}

CCF.prsiteconfig.postSiteConfig = function() {

	XNAT.xhr.put({
    		url: XNAT.url.rootUrl('/data/config/projectReportsSiteConfig/configuration'),
    		data: JSON.stringify(CCF.prsiteconfig.configuration),
    		contentType: 'text/plain',
    		success: function(prcJson){
			CCF.prsiteconfig.populateTable();
		},
    		failure: function(failData){
			console.log("ERROR POSTING projectReportsSiteConfig CONFIGURATION");
			console.log(failData);
		}
	});

}

CCF.prsiteconfig.populateTable = function() {

	if (CCF.prsiteconfig.configuration.length==0) {
		$(CCF.prsiteconfig.currentTag).html("No reports are available to be configured.");
		return;
	}
	var tableHtml = "<table class='data-table xnat-table' style='width:100%;'>";
	tableHtml+="<tr><th></th><th class='center'>Enabled</th><th class='center'>Namespace</th><th class='center'>Description</th></tr>";
	CCF.prsiteconfig.configuration.sort(function(a,b) {
		return a.namespace.replace(/^.*:/,"").localeCompare(b.namespace.replace(/^.*:/,""));
	})
	for (var i=0; i<CCF.prsiteconfig.configuration.length; i++) {
		var prsiteconfig = CCF.prsiteconfig.configuration[i];
		tableHtml+="<tr>";
		tableHtml+="<td class='center'><a href='javascript:CCF.prsiteconfig.reportInfoPanel(\"" + prsiteconfig.namespace + "\");'>View/Edit</a></td>";
		tableHtml+="<td>" + prsiteconfig.enabled + "</td>";
		tableHtml+="<td>" + prsiteconfig.namespace + "</td>";
		tableHtml+="<td>" + prsiteconfig.description + "</td>";
		tableHtml+="</tr>";
	}
	tableHtml+="</table>";
	$(CCF.prsiteconfig.currentTag).html(tableHtml);

}

CCF.prsiteconfig.reportInfoPanel = function(namespace) {

	CCF.prsiteconfig.veLoaded = false;

	xmodal.open({
		title: 'View/Edit Report Configuration',
		id: "view-modal",
		width: 700,
		height: 530,
		content: '<div id="viewReportConfigDiv"></div>',
		okLabel: 'Submit',
		okClose: false,
		cancel: 'Cancel',
		cancelAction: (function() {
			// Need to revert any values changed
			CCF.prsiteconfig.populateTable();
			xmodal.closeAll();
		}),
		okAction: (function() {
			var enabled = $("[name=input-enabled]").val();
			var namespace = $("[name=input-namespace]").val();
			var description = $("[name=input-description]").val();
			var projectList = $("[name=input-projectList]").val();
			var notOk = false;
			for (var i=0; i<CCF.prsiteconfig.configuration.length; i++) {
				var config = CCF.prsiteconfig.configuration[i];
				if (config.namespace==namespace) {
					config.enabled = enabled;
					config.description = description;
					config.projectList = projectList;
					if ((enabled == 'true') && (typeof description === 'undefined' || description.length<1)) {
						notOk = true;
						XNAT.ui.banner.top(3000, 'You must provide a description for enabled reports.');
						
					}
					break;
				}
			}
			if (!notOk) {
				XNAT.ui.banner.top(2000, 'Report configuration updated.');
				CCF.prsiteconfig.postSiteConfig();
				xmodal.closeAll();
			}
		})
	});

	XNAT.spawner.spawn(CCF.prsiteconfig.viewConfig(namespace)).render($("#viewReportConfigDiv")[0]);

}


CCF.prsiteconfig.viewConfig = function(namespace) {

	var reportInfo = {};
	for (var i=0; i<CCF.prsiteconfig.configuration.length; i++) {
		var config = CCF.prsiteconfig.configuration[i];
		if (config.namespace==namespace) {
			reportInfo = CCF.prsiteconfig.configuration[i];
			break;
		}
	}

	var viewConfig = {
		viewPanel: {
			kind: 'panel',
			header: false,
			footer: false,
			contents: {
				enabled: {
					kind: 'panel.input.switchbox',
					name: 'input-enabled',
					className: 'required prsiteconfig-ele',
					onText: 'Enabled',
					offText: 'Disabled',
					label: 'Report Enabled',
					value: reportInfo.enabled
				},
				reportNamespace: {
					kind: 'panel.input.text',
					name: 'input-namespace',
					className: 'required prsiteconfig-ele',
					size: '30',
					label: 'Report Namespace',
					element: { disabled : 'disabled' },
					description: "Namespace of the YAML file that specifies the report.",
					value: reportInfo.namespace
				},
				description: {
					kind: 'panel.input.text',
					name: 'input-description',
					className: 'required prsiteconfig-ele',
					size: '30',
					label: 'Description',
					value: reportInfo.description
				},
				projectList: {
					kind: 'panel.select.multi',
					name: 'input-projectList',
					className: 'required prsiteconfig-ele',
					label: 'Project List',
					options: CCF.prsiteconfig.projectList,
					element: { style: { 'width' : '200px', 'height' : '150px' } },
					description: 'When projects are specified, only those specifically allowed will be allowed to configure this report.  ' +
							'Otherwise, all projects will be allowed to configure the report.',
					value: reportInfo.projectList
				},
			}
		}
	};

	return viewConfig;

}

