
if (typeof CCF === 'undefined') {
	CCF = {};
}

if (typeof CCF.prconfig === 'undefined') {
	CCF.prconfig = { };
}

CCF.prconfig.populateProjectReportsTable = function(tag) {

	var allowedConfig = [];
	CCF.prconfig.projectConfig = [];
	CCF.prconfig.currentProject = "";
	CCF.prconfig.currentTag = tag;
	var queryParams = new URLSearchParams(window.location.search)
	XNAT.xhr.getJSON({
    		url: XNAT.url.rootUrl('/data/config/projectReportsSiteConfig/configuration?contents=true'),
    		success: function(configJson){
			CCF.prconfig.currentProject = queryParams.get("id");
			for (var i=0; i<configJson.length; i++) {
				var rptConfig = configJson[i];
				if (typeof rptConfig.namespace !== 'undefined' && typeof rptConfig.enabled !== 'undefined' && 
					typeof rptConfig.projectList !== 'undefined') {
					if (rptConfig.enabled == 'true') {
						if ((!$.isArray(rptConfig.projectList)) || rptConfig.projectList.length<1 ||
							 rptConfig.projectList.includes(CCF.prconfig.currentProject)) {
							var configInfo = {};
							configInfo.namespace = rptConfig.namespace;
							configInfo.description = rptConfig.description;
							allowedConfig.push(configInfo);
						}
					}
				}
			}
			XNAT.xhr.getJSON({
		    		url: XNAT.url.rootUrl('/data/projects/' + CCF.prconfig.currentProject + '/config/projectReportsProjectConfig/configuration?contents=true'),
		    		success: function(pConfigJson){
					for (var i=0; i<pConfigJson.length; i++) {
						var projConfig = pConfigJson[i];
						var hasMatch = false;
						for (var j=0; j<allowedConfig.length; j++) {
							if (projConfig.namespace==allowedConfig[j].namespace) {
								projConfig.description = allowedConfig[j].description;
								hasMatch = true;
							}
						}
						if (hasMatch) {
							CCF.prconfig.projectConfig.push(projConfig);
						}
					}
					for (var i=0; i<allowedConfig.length; i++) {
						var aConfig = allowedConfig[i];
						var hasMatch = false;
						for (var j=0; j<CCF.prconfig.projectConfig.length; j++) {
							if (aConfig.namespace==CCF.prconfig.projectConfig[j].namespace) {
								hasMatch = true;
								break;
							}
						}
						if (!hasMatch) {
							var newConfig = { enabled: false,
									  namespace: aConfig.namespace,
									  description: aConfig.description
									};
							CCF.prconfig.projectConfig.push(newConfig);
						}
					}
					CCF.prconfig.populateTable();
				},
		    		failure: function(failData){
					for (var i=0; i<allowedConfig.length; i++) {
						var aConfig = allowedConfig[i];
						var newConfig = { enabled: false,
								  namespace: aConfig.namespace,
								  description: aConfig.description
								};
						CCF.prconfig.projectConfig.push(newConfig);
					}
					CCF.prconfig.populateTable();
				}
			})
		},
    		failure: function(failData){
			$(tag).html("<h3>ERROR:  Could not retrieve site-level report plugins configuration information</h3>");
		}
	})

}

CCF.prconfig.populateTable = function() {

	if (CCF.prconfig.projectConfig.length==0) {
		$(CCF.prconfig.currentTag).html("No reports are available to be configured.");
		return;
	}
	var tableHtml = "<table class='data-table xnat-table' style='width:100%;'>";
	//tableHtml+="<tr><th></th><th class='center'>Enabled</th><th></th><th class='center'>Report Description</th></tr>";
	tableHtml+="<tr><th class='center'>Status</th><th class='center'>Report Description</th></tr>";
	CCF.prconfig.projectConfig.sort(function(a,b) {
		return a.namespace - b.namespace;
	})
	for (var i=0; i<CCF.prconfig.projectConfig.length; i++) {
		var prconfig = CCF.prconfig.projectConfig[i];
		tableHtml+="<tr>";
		tableHtml+="<td><div id='td-enabled-" + prconfig.namespace.replace(":","") + "'></div></td>";
		tableHtml+="<td>" + prconfig.description + "</td>";
		tableHtml+="</tr>";
	}
	tableHtml+="</table>";
	$(CCF.prconfig.currentTag).html(tableHtml);
	setTimeout(function() {
		for (var i=0; i<CCF.prconfig.projectConfig.length; i++) {
			var prconfig = CCF.prconfig.projectConfig[i];
			var enabledSwitch = {
					enabled: {
						kind: 'panel.input.switchbox',
						name: 'input-enabled-' + prconfig.namespace,
						className: 'required prconfig-ele',
						onText: 'Enabled',
						offText: 'Disabled',
						label: ' ',
                                		element: { 
								onchange: function(e){
									setTimeout(function() {
										var eleNs = $(e.srcElement).attr("title").replace("input-enabled-","");
										var eleVal = $(e.srcElement).val();
										for (var i=0; i<CCF.prconfig.projectConfig.length; i++) {
											var config = CCF.prconfig.projectConfig[i];
											if (config.namespace==eleNs) {
												config.enabled = eleVal;
												break;
											}
										}
									CCF.prconfig.postProjectConfig();
									}, 100);
                                				},
						},
						value: prconfig.enabled
					}
				};
			XNAT.spawner.spawn(enabledSwitch).render($("#td-enabled-" + prconfig.namespace.replace(":",""))[0]);
		}
	}, 50);
}


CCF.prconfig.postProjectConfig = function() {

	XNAT.xhr.put({
    		url: XNAT.url.rootUrl('/data/projects/' + CCF.prconfig.currentProject + '/config/projectReportsProjectConfig/configuration'),
    		data: JSON.stringify(CCF.prconfig.projectConfig),
    		contentType: 'text/plain',
    		success: function(prcJson){
			XNAT.ui.banner.top(2000, 'Report configuration updated.');
		},
    		failure: function(failData){
			console.log("ERROR POSTING projectReportsProjectConfig CONFIGURATION");
			console.log(failData);
			CCF.prconfig.populateProjectReportsTable();
		}
	});

}



CCF.prconfig.viewConfig = function(namespace) {

	var reportInfo = {};
	for (var i=0; i<CCF.prconfig.projectConfig.length; i++) {
		var config = CCF.prconfig.projectConfig[i];
		if (config.namespace==namespace) {
			reportInfo = CCF.prconfig.projectConfig[i];
			break;
		}
	}

	var viewConfig = {
		viewPanel: {
			kind: 'panel',
			header: false,
			footer: false,
			contents: {
				enabled: {
					kind: 'panel.input.switchbox',
					name: 'input-enabled',
					className: 'required prconfig-ele',
					onText: 'Enabled',
					offText: 'Disabled',
					label: 'Report Enabled',
					value: reportInfo.enabled
				},
				description: {
					kind: 'panel.input.text',
					name: 'input-description',
					className: 'required prconfig-ele',
					size: '30',
					element: { disabled : 'disabled' },
					label: 'Description',
					value: reportInfo.description
				},
			}
		}
	};

	return viewConfig;

}





