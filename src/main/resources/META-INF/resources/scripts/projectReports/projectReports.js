
//# sourceURL=projectReports/projectReports.js

if (typeof CCF === 'undefined') {
	CCF = { };
}

if (typeof CCF.pr === 'undefined') {
	CCF.pr = { 
		isInitialized : false,
		namespaces : [],
		message: "<h3>No reports have been configured for this project</h3>"
	};
}

CCF.pr.open = function(project) {

	window.location.assign(serverRoot + "/app/template/Page.vm?view=project-reports&project=" + project);

}

CCF.pr.initialize = function(project, doPopulate) {

	CCF.pr.isInitialized = true;
	console.log("Project reports initialization called.");
	var allowedConfig = [];
	CCF.pr.configNS = [];
	CCF.pr.queryParams = new URLSearchParams(window.location.search)
	XNAT.xhr.getJSON({
    		url: XNAT.url.rootUrl('/data/config/projectReportsSiteConfig/configuration?contents=true'),
    		success: function(configJson){
			for (var i=0; i<configJson.length; i++) {
				var rptConfig = configJson[i];
				if (typeof rptConfig.namespace !== 'undefined' && typeof rptConfig.enabled !== 'undefined' && 
					typeof rptConfig.projectList !== 'undefined') {
					if (rptConfig.enabled == 'true') {
						if ((!$.isArray(rptConfig.projectList)) || rptConfig.projectList.length<1 ||
							 rptConfig.projectList.includes(project)) {
							var configInfo = {};
							configInfo.namespace = rptConfig.namespace;
							configInfo.description = rptConfig.description;
							allowedConfig.push(configInfo);
						}
					}
				}
			}
			XNAT.xhr.getJSON({
		    		url: XNAT.url.rootUrl('/data/projects/' + project + '/config/projectReportsProjectConfig/configuration?contents=true'),
		    		success: function(pConfigJson){
					for (var i=0; i<pConfigJson.length; i++) {
						var projConfig = pConfigJson[i];
						if (typeof projConfig.enabled === 'undefined' || !(projConfig.enabled == 'true')) {
							continue;
						}
						var hasMatch = false;
						for (var j=0; j<allowedConfig.length; j++) {
							if (projConfig.namespace==allowedConfig[j].namespace) {
								projConfig.description = allowedConfig[j].description;
								hasMatch = true;
							}
						}
						if (hasMatch) {
							CCF.pr.configNS.push(projConfig.namespace);
						}
					}

					var items = [];
					XNAT.xhr.getJSON({
			    			url: XNAT.url.rootUrl('/xapi/spawner/namespaces'),
			    			success: function(json){
							json.sort(function(a,b) {
									a = a.replace(/^.*:/,"");
									b = b.replace(/^.*:/,"");
									if (a>b) return 1;
									else if (a<b) return -1;
									else return 0;
								});
							var hasReport = false;
			        			$.each(json, function(){
								var NS = this.toString();
								if (NS.includes(":projectReport") && CCF.pr.configNS.includes(NS)) {
									hasReport = true;
									CCF.pr.namespaces.push(NS);
									//CCF.pr.spawnReport(NS);
								}
							});
							if (hasReport) {
								$(".prLink").removeClass("hidden");
								
							}
							if (typeof doPopulate !== 'undefined' && doPopulate === true) {
								CCF.pr.populate(project);
							}
							if (CCF.pr.queryParams.get("project") == project) {
								$(".xnat-nav-tabs").css('width','15%')
								$(".xnat-tab-content.side").css({ "width": "85%", "padding-left": "15px", "padding-right": "15px"})
								$("#layout-context").css({ "width": "1135px", "margin-left": "10px", "margin-right": "10px"})
								$(".xnat-tab-container").css({ "width": "1135px", "margin-left": "10px", "margin-right": "10px"})
							}
							console.log("Project reports initialization completed.");
						},
    						failure: function(failData){
							CCF.pr.message = "<h3>ERROR:  Couldn't get namespace information for project reports</h3>";
							if (typeof doPopulate !== 'undefined' && doPopulate === true) {
								CCF.pr.populate(project);
							}
							console.log(failData);
						},
					});
				},
    				failure: function(failData){
					if (failData.status == 403) {
						CCF.pr.message = "<h3>ERROR:  You don't have permission to view reports for this project</h3>";
						if (typeof doPopulate !== 'undefined' && doPopulate === true) {
							CCF.pr.populate(project);
						}
						return;
					}
					CCF.pr.message = "<h3>ERROR:  Couldn't get project reports project-level configuration</h3>";
					if (typeof doPopulate !== 'undefined' && doPopulate === true) {
						CCF.pr.populate(project);
					}
					console.log(failData);
				},
			});
		},
    		failure: function(failData){
			CCF.pr.message = "<h3>ERROR:  Couldn't get project reports site configuration.</h3>";
			if (typeof doPopulate !== 'undefined' && doPopulate === true) {
				CCF.pr.populate(project);
			}
			console.log(failData);
		},
	});
}

CCF.pr.populate = function(project) {
	if (CCF.pr.isInitialized !== true) {
		CCF.pr.initialize(project, true);
	} else {
		var hasReport = false;
		$.each(CCF.pr.namespaces, function(){
			var NS = this;
			hasReport = true;
			CCF.pr.spawnReport(NS);
		});
		if (!hasReport) {
			$("#project-reports-tabs").html(CCF.pr.message);
		}
	}
}

CCF.pr.spawnReport = function(namespace) {
	XNAT.xhr.getJSON({
    		url: XNAT.url.rootUrl('/xapi/spawner/ids/' + namespace),
    		async: false,
    		success: function(json){
				json.sort();
        			$.each(json, function(){
					XNAT.xhr.getJSON({
				    		url: XNAT.url.rootUrl('/xapi/spawner/resolve/' + namespace + "/" + this),
    						async: false,
				    		success: function(json2){
				        			$.each(json2, function(){
									var key = (Object.keys(json2))[0];
									if (typeof json2[key].kind !== 'undefined' && json2[key].kind === 'tabs') {
										try {
											XNAT.spawner.spawn(json2).render($("#project-reports-tabs").find("div.content-tabs"));
										} catch(e) {
											console.log("ERROR Spawning report (NAMESPACE=" + namespace + ").  Note this could be a problem with " +
												"with the object generated from YAML, but it could also be a problem with JavaScript included " +
                                                                                                "by a script tag in the spawned output.");
											console.log(JSON.stringify(json2));
											console.log(json2);
											throw e;
										}
									}
								});
							}
					});
				
				});
			}
	});
}

