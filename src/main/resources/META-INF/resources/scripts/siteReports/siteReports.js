
if (typeof CCF === 'undefined') {
	CCF = { };
}

if (typeof CCF.sr === 'undefined') {
	CCF.sr = { 
		isInitialized : false,
		isInitializing : false,
		namespaces : [],
		message: "<h3>No site-level reports have been configured</h3>"
	};
}

CCF.sr.open = function() {

	window.location.assign(serverRoot + "/app/template/Page.vm?view=site-reports");

}

CCF.sr.initialize = function(doPopulate) {

	if (CCF.sr.isInitializing == true || CCF.sr.isInitialized == true) {
		return;
	}
	console.log("Site reports initialization called");
	CCF.sr.isInitializing = true;
	var allowedNamespaces = [];
	XNAT.xhr.getJSON({
    		url: XNAT.url.rootUrl('/data/config/siteReportsConfig/configuration?contents=true'),
		async: false,
    		success: function(configJson){
			for (var i=0; i<configJson.length; i++) {
				var rptConfig = configJson[i];
				if (typeof rptConfig.namespace !== 'undefined' && typeof rptConfig.enabled !== 'undefined') {
					if (rptConfig.enabled == 'true') {
						var configInfo = {};
						configInfo.namespace = rptConfig.namespace;
						configInfo.description = rptConfig.description;
						allowedNamespaces.push(configInfo.namespace);
					}
				}
			}
			var items = [];
			XNAT.xhr.getJSON({
		 		url: XNAT.url.rootUrl('/xapi/spawner/namespaces'),
				async: false,
		    		success: function(json){
					json.sort();
					var hasReport = false;
		       			$.each(json, function(){
						var NS = this.toString();
						if (NS.includes(":siteReport") && allowedNamespaces.includes(NS) && !CCF.sr.namespaces.includes(NS)) {
							hasReport = true;
							CCF.sr.namespaces.push(NS);
							//CCF.sr.spawnReport(NS);
						}
					});
					if (hasReport) {
						$(".srLink").removeClass("hidden");
						
					}
					CCF.sr.isInitializing = false;
					CCF.sr.isInitialized = true;
					if (typeof doPopulate !== 'undefined' && doPopulate === true) {
						CCF.sr.populate();
					}
					setTimeout(function() {
						$(".xnat-nav-tabs").css('width','15%')
						$(".xnat-tab-content.side").css({ "width": "85%", "padding-left": "15px", "padding-right": "15px"})
						$("#layout-context").css({ "width": "1135px", "margin-left": "10px", "margin-right": "10px"})
						$(".xnat-tab-container").css({ "width": "1135px", "margin-left": "10px", "margin-right": "10px"})
                                        }, 100);
					console.log("Site reports initialization finished");
				},
    				failure: function(failData){
					CCF.sr.message = "<h3>ERROR:  Couldn't get namespace information for site reports</h3>";
					CCF.sr.isInitializing = false;
					CCF.sr.isInitialized = true;
					if (typeof doPopulate !== 'undefined' && doPopulate === true) {
						CCF.sr.populate();
					}
					console.log(failData);
				},
			});
		},
    		failure: function(failData){
			CCF.sr.message = "<h3>ERROR:  Couldn't get site reports configuration.</h3>";
			CCF.sr.isInitializing = false;
			CCF.sr.isInitialized = true;
			if (typeof doPopulate !== 'undefined' && doPopulate === true) {
				CCF.sr.populate();
			}
			console.log(failData);
		},
	});
}

CCF.sr.populate = function() {
	var counter = 0;
	if (CCF.sr.isInitializing == false && CCF.sr.isInitialized == false) {
		CCF.sr.initialize();
	}
	var hasReport = false;
	$.each(CCF.sr.namespaces, function(){
		var NS = this;
		hasReport = true;
		CCF.sr.spawnReport(NS);
	});
	if (!hasReport) {
		$("#site-reports-tabs").html(CCF.sr.message);
	}
}

CCF.sr.spawnReport = function(namespace) {
	XNAT.xhr.getJSON({
    		url: XNAT.url.rootUrl('/xapi/spawner/ids/' + namespace),
    		async: false,
    		success: function(json){
				json.sort();
        			$.each(json, function(){
					XNAT.xhr.getJSON({
				    		url: XNAT.url.rootUrl('/xapi/spawner/resolve/' + namespace + "/" + this),
				    		success: function(json2){
				        			$.each(json2, function(){
									var key = (Object.keys(json2))[0];
									if (typeof json2[key].kind !== 'undefined' && json2[key].kind === 'tabs') {
										XNAT.spawner.spawn(json2).render($("#site-reports-tabs").find("div.content-tabs"));
									}
								});
							}
					});
				
				});
			}
	});
}

