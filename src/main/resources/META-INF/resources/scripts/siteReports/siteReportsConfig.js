
if (typeof CCF === 'undefined') {
	CCF = {};
}

if (typeof CCF.srconfig === 'undefined') {
	CCF.srconfig = { };
}

CCF.srconfig.populateScriptsSiteConfigTable = function(tag) {

	CCF.srconfig.currentTag = tag;
	CCF.srconfig.siteReportsNS = [];
	CCF.srconfig.configuredNS = [];

	XNAT.xhr.getJSON({
    		url: XNAT.url.rootUrl('/xapi/spawner/namespaces'),
    		success: function(nsJson){
			nsJson.sort();
       			$.each(nsJson, function(){
				var NS = this.toString();
				if (NS.includes(":siteReport")) {
					CCF.srconfig.siteReportsNS.push(NS);
				}
			});
			XNAT.xhr.getJSON({
    				url: XNAT.url.rootUrl('/data/config/siteReportsConfig/configuration?contents=true'),
    				success: function(configJson){
					var removed = true;
					outerLoop:
					while (removed) {
						for (var i=0; i<configJson.length; i++) {
							var rptConfig = configJson[i];
							if (typeof rptConfig.namespace !== 'undefined') {
								if (!CCF.srconfig.siteReportsNS.includes(rptConfig.namespace)) {
									configJson.splice(i,1);
									break outerLoop;
								}
							}
						} 
						removed = false;
					}
					var added = false;
					for (var i=0; i<CCF.srconfig.siteReportsNS.length; i++) {
						var prNS = CCF.srconfig.siteReportsNS[i];
						var hasMatch = false;
						for (var j=0; j<configJson.length; j++) {
							var rptConfig = configJson[j];
							if (typeof rptConfig.namespace !== 'undefined' && rptConfig.namespace == prNS) {
								hasMatch = true;
								break;
							}
						} 
						if (!hasMatch) {
							var newConfig = { enabled: false, namespace: prNS, description: "" };
							added = true;
							configJson.push(newConfig);
						}
					}
					if (added) {
						CCF.srconfig.updateSiteConfig(configJson);
					} else {
						CCF.srconfig.configuration = configJson;
						CCF.srconfig.populateTable();
					}
				},
    				failure: function(failData){
					if (failData.status==404) {
						CCF.srconfig.initSiteConfig();
						CCF.srconfig.populateTable();
					}
				}
			});
		}
	});

}

CCF.srconfig.initSiteConfig = function() {

	CCF.srconfig.configuration = [];
       	$.each(CCF.srconfig.siteReportsNS, function(){
		var NS = this;
		var config = { enabled: false, namespace: NS, description: "" };
		CCF.srconfig.configuration.push(config);
	});
	CCF.srconfig.postSiteConfig();

}

CCF.srconfig.updateSiteConfig = function(configJson) {

	CCF.srconfig.configuration = configJson;
	CCF.srconfig.postSiteConfig();

}

CCF.srconfig.postSiteConfig = function() {

	XNAT.xhr.put({
    		url: XNAT.url.rootUrl('/data/config/siteReportsConfig/configuration'),
    		data: JSON.stringify(CCF.srconfig.configuration),
    		contentType: 'text/plain',
    		success: function(prcJson){
			CCF.srconfig.populateTable();
		},
    		failure: function(failData){
			console.log("ERROR POSTING siteReportsConfig CONFIGURATION");
			console.log(failData);
		}
	});

}

CCF.srconfig.populateTable = function() {

	if (CCF.srconfig.configuration.length==0) {
		$(CCF.srconfig.currentTag).html("No reports are available to be configured.");
		return;
	}
	var tableHtml = "<table class='data-table xnat-table' style='width:100%;'>";
	tableHtml+="<tr><th></th><th class='center'>Enabled</th><th class='center'>Namespace</th><th class='center'>Description</th></tr>";
	CCF.srconfig.configuration.sort(function(a,b) {
		return a.namespace - b.namespace;
	})
	for (var i=0; i<CCF.srconfig.configuration.length; i++) {
		var srconfig = CCF.srconfig.configuration[i];
		tableHtml+="<tr>";
		tableHtml+="<td class='center'><a href='javascript:CCF.srconfig.reportInfoPanel(\"" + srconfig.namespace + "\");'>View/Edit</a></td>";
		tableHtml+="<td>" + srconfig.enabled + "</td>";
		tableHtml+="<td>" + srconfig.namespace + "</td>";
		tableHtml+="<td>" + srconfig.description + "</td>";
		tableHtml+="</tr>";
	}
	tableHtml+="</table>";
	$(CCF.srconfig.currentTag).html(tableHtml);

}

CCF.srconfig.reportInfoPanel = function(namespace) {

	CCF.srconfig.veLoaded = false;

	xmodal.open({
		title: 'View/Edit Report Configuration',
		id: "view-modal",
		width: 700,
		height: 530,
		content: '<div id="viewSiteReportConfigDiv"></div>',
		okLabel: 'Submit',
		okClose: false,
		cancel: 'Cancel',
		cancelAction: (function() {
			// Need to revert any values changed
			CCF.srconfig.populateTable();
			xmodal.closeAll();
		}),
		okAction: (function() {
			var enabled = $("[name=input-enabled]").val();
			var namespace = $("[name=input-namespace]").val();
			var description = $("[name=input-description]").val();
			var notOk = false;
			for (var i=0; i<CCF.srconfig.configuration.length; i++) {
				var config = CCF.srconfig.configuration[i];
				if (config.namespace==namespace) {
					config.enabled = enabled;
					config.description = description;
					if ((enabled == 'true') && (typeof description === 'undefined' || description.length<1)) {
						notOk = true;
						XNAT.ui.banner.top(3000, 'You must provide a description for enabled reports.');
						
					}
					break;
				}
			}
			if (!notOk) {
				XNAT.ui.banner.top(2000, 'Site configuration updated.');
				CCF.srconfig.postSiteConfig();
				xmodal.closeAll();
			}
		})
	});

	XNAT.spawner.spawn(CCF.srconfig.viewConfig(namespace)).render($("#viewSiteReportConfigDiv")[0]);

}


CCF.srconfig.viewConfig = function(namespace) {

	var reportInfo = {};
	for (var i=0; i<CCF.srconfig.configuration.length; i++) {
		var config = CCF.srconfig.configuration[i];
		if (config.namespace==namespace) {
			reportInfo = CCF.srconfig.configuration[i];
			break;
		}
	}

	var viewConfig = {
		viewPanel: {
			kind: 'panel',
			header: false,
			footer: false,
			contents: {
				enabled: {
					kind: 'panel.input.switchbox',
					name: 'input-enabled',
					className: 'required srconfig-ele',
					onText: 'Enabled',
					offText: 'Disabled',
					label: 'Report Enabled',
					value: reportInfo.enabled
				},
				reportNamespace: {
					kind: 'panel.input.text',
					name: 'input-namespace',
					className: 'required srconfig-ele',
					size: '30',
					label: 'Report Namespace',
					element: { disabled : 'disabled' },
					description: "Namespace of the YAML file that specifies the report.",
					value: reportInfo.namespace
				},
				description: {
					kind: 'panel.input.text',
					name: 'input-description',
					className: 'required srconfig-ele',
					size: '30',
					label: 'Description',
					value: reportInfo.description
				}
			}
		}
	};

	return viewConfig;

}

