package org.nrg.ccf.reporting.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "ccfReportingPlugin",
			name = "CCF Reporting Plugin",
			log4jPropertiesFile="/META-INF/resources/reportingLog4j.properties"
		)
@ComponentScan({ 
		"org.nrg.ccf.reporting.conf",
		"org.nrg.ccf.reporting.xapi"
	})
public class CcfReportingPlugin {
	
	public static Logger logger = Logger.getLogger(CcfReportingPlugin.class);

	public CcfReportingPlugin() {
		logger.info("Configuring the CCF Reporting Plugin.");
	}
	
}
